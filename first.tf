provider "aws" {
  region = "eu-west-3"
}

resource "aws_instance" "example" {
  # ubuntu
  # ami                    = "ami-08182c55a1c188dee"
  # redhat
  ami = "ami-5026902d"

  # centos 7
  ami                    = "ami-262e9f5b"
  instance_type          = "t2.micro"
  key_name               = "${aws_key_pair.default.id}"
  vpc_security_group_ids = ["${aws_security_group.default.id}"]

  # max micro with aws = 20
  count = 1
}

resource "aws_key_pair" "default" {
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1ywJq588bAVJAjeleliFZbqUQ8kyRatFzJ5pfxqDeWD9BM2obKnpMTHR0A9D4zpulEKIogfE83gBqQggpLba6URMAhWN1u7lIQDHQBuckuspwJzai8qJgatCY0/6aBE54q1uq8FIW9Hm3UPkNkaBg3jtXeRAcG6uRtLpy4k/WmdekxcBl3j8E+fk7ARui2UNUc9P5BIYx3UANwLU5z3DwgKeDPoBaJG0pvluFOO5jFNWgCLhhPtvMbTYck0QR77nqhlDkHSymM2jkGnMujNf9Ho6LIeQpsCkU0jMfKvtHf1h99gMNPpg03BbxeW4iWqHxUHJxWjic6yK43zh7dkMF emilien.mottet@grenoble-inp.org"
}

resource "aws_security_group" "default" {
  name = "terraform-test"

  # Allow SSH & HTTP in
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "SSH in"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "HTTP in"
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Enable ICMP
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
