# Déployer Glances

## install glances
```
pip install --user cassandra-driver
pip install git+https://github.com/nicolargo/glances@develop --user
```

## Deploy on local

edit your ```~/.config/glances``` or ```/etc/glances```

```ini
[kafka]
host=localhost
port=9092
topic=glances
#compression=gzip
```

## Deploy on ec2

## Prérequis

* Installer [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
* Installer [Terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)

Exécuter les commandes suivantes :

```bash
ansible-galaxy install zaxos.glances-ansible-role
go get github.com/adammck/terraform-inventory
terraform init
```

Ne pas oublier de mettre votre clé SSH publique dans le `first.tf` de manière à pouvoir accéder aux machines créées.

## Déploiement avec ansible-galaxy
```
terraform apply
ansible-playbook -u centos -i $(which terraform-inventory) test.yml
```
use the good user with -u (https://alestic.com/2014/01/ec2-ssh-username/)

## Déploiement avec notre script ansible
recomander pour cassandra
```
terraform apply
ansible-playbook -u centos -i $(which terraform-inventory) our-playbook.yml
```

### Démarrer Glances

Editez la configuration de Glances présente dans le dossier `glances.conf`, en fonction de vos besoins (Kafka ou Cassandra).
